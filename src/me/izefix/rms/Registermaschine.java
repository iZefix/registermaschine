/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.izefix.rms;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Brosch.Dominik
 */
public class Registermaschine extends Application {

    public static Stage stage;

    @Override
    public void start(Stage stage) {
        try {
            Registermaschine.stage = stage;
            URL url = getClass().getResource("Interface.fxml");
            //System.out.println("Loading from " + url);
            Parent root = FXMLLoader.load(url);
            Scene scene = new Scene(root, 666, 500);
            scene.getStylesheets().add("/me/izefix/rms/interface.css");
            stage.setTitle("RegisterMaschine");
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(Registermaschine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
