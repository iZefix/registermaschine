/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.izefix.rms;

/**
 *
 * @author dbrosch
 */
public class Command {

    private CommandType t;
    private int arg;

    public Command(CommandType t, int arg) {
        this.t = t;
        this.arg = arg;
    }

    public CommandType getType() {
        return t;
    }

    public int getArgument() {
        return arg;
    }

    public static enum CommandType {

        DLOAD("Lädt einen Wert in den Akkumulator"),
        LOAD("Lädt ein Register in den Akkumulator"),
        STORE("Speichert den Akkumulator in ein Register"),
        ADD("Addiert ein Register zum Akk"),
        SUB("Subtrahiert ein Register vom Akk"),
        END("Beendet das Programm", false),
        MULT("Multipliziert ein Register mit dem Akk"),
        DIV("Dividiert den Akk durch ein Register"),
        JGE("Setzt den Befehlszähler neu, falls Akk>=0"),
        JGT("Setzt den BZ neu, falls Akk>0"),
        JLE("Springt, falls Akk<=0"),
        JLT("Springt, falls Akk<0"),
        JEQ("Springt, falls Akk==0"),
        JNE("Springt, falls Akk!=0"),
        JUMP("Springt zu Zeile"),
        TRUMP("Der Präsident");

        private String description;
        private boolean reqArgument;

        CommandType(String description) {
            this(description, true);
        }

        private CommandType(String description, boolean reqArgument) {
            this.description = description;
            this.reqArgument = reqArgument;
        }

        public String getDescription() {
            return description;
        }

        public boolean doesRequireArgument() {
            return reqArgument;
        }

        public static CommandType safeValueOf(String name) {
            try {
                return CommandType.valueOf(name);
            } catch (Exception ex) {

            }
            return null;
        }

    }

}
