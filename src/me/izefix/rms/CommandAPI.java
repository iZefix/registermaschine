/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.izefix.rms;

/**
 *
 * @author dbrosch
 */
public class CommandAPI {

    private InterfaceController controller;
    private static CommandAPI instance;

    private CommandAPI(InterfaceController controller) {
        this.controller = controller;
    }

    public static void init(InterfaceController controller) {
        if (controller != null) {
            instance = new CommandAPI(controller);
        } else {
            throw new IllegalArgumentException("The Controller must not be null.");
        }
    }

    public static boolean isInitialized() {
        return instance != null;
    }

    public static boolean isValidReg(int register) {
        return register > 0 && instance.controller.regs.length > register;
    }

    public static int getRegEntry(int register) {
        if (isValidReg(register)) {
            try {
                return Integer.parseInt(instance.controller.regs[register].getText());
            } catch (NumberFormatException ex) {
                throw new IllegalStateException("Register is not initialized.");
            }
        } else {
            throw new IllegalArgumentException("Register is not valid.");
        }
    }

    public static boolean hasRegEntry(int register) {
        try {
            getRegEntry(register);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public static void updateReg(int register, int value) {

    }

}
