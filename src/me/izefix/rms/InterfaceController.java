/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.izefix.rms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import me.izefix.rms.Command.CommandType;

/**
 * FXML Controller class
 *
 * @author Brosch.Dominik
 */
public class InterfaceController implements Initializable {

    @FXML
    public TextArea editor;

    @FXML
    private AnchorPane root;

    @FXML
    public Label akk;

    @FXML
    public Label bz;

    @FXML
    private Label r0;

    @FXML
    private Label r1;
    @FXML
    private Label r2;
    @FXML
    private Label r3;
    @FXML
    private Label r4;
    @FXML
    private Label r5;
    @FXML
    private Label r6;
    @FXML
    private Label r7;
    @FXML
    private Label r8;
    @FXML
    private Label r9;
    @FXML
    private Label r10;
    @FXML
    private Label r11;

    @FXML
    private Button importBtn;

    @FXML
    private Button exportBtn;

    @FXML
    private Button stepBtn;

    @FXML
    private Button fullBtn;

    @FXML
    private Label error;

    @FXML
    private ScrollPane code_run_scroll;

    @FXML
    private VBox code_run;

    public Label[] regs = new Label[12];
    private Command[] commands;
    private Label[] cmd_display;
    private Label current = null;
    private File loaded = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        regs[0] = r0;
        regs[1] = r1;
        regs[2] = r2;
        regs[3] = r3;
        regs[4] = r4;
        regs[5] = r5;
        regs[6] = r6;
        regs[7] = r7;
        regs[8] = r8;
        regs[9] = r9;
        regs[10] = r10;
        regs[11] = r11;
        reset();
        editor.setText("1: ");
        editor.positionCaret(editor.getText().length());
        editor.setOnKeyPressed((KeyEvent event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                if (event.isAltDown()) {
                    editor.insertText(editor.getCaretPosition(), "\n");
                } else {
                    int cursor = editor.getCaretPosition();
                    String prev = editor.getText().substring(0, cursor);
                    int line = prev.split("\n").length + 1;
                    String add = "\n" + line + ": ";
                    if (editor.getText().length() == editor.getCaretPosition()) {
                        editor.setText(editor.getText() + add);
                    } else {
                        String original = editor.getText(cursor, editor.getText().length());
                        StringBuilder b = new StringBuilder();
                        b.append(prev);
                        b.append(add);
                        String[] lines = original.split("\n");
                        for (int i = 0; i < lines.length; i++) {
                            if (lines[i].contains(": ")) {
                                String[] split = lines[i].split(": ", 2);
                                b.append(line);
                                b.append(": ");
                                if (split.length == 2) {
                                    b.append(split[1]);
                                }
                            } else if (i > 0) {
                                b.append(line).append(": ").append(lines[i]);
                            }
                            line++;
                            if (i < lines.length - 1) {
                                b.append("\n");
                            }
                        }
                        editor.setText(b.toString());
                    }
                    editor.positionCaret(cursor + add.length());
                    event.consume();
                }
                return;
            }
            if (event.getCode() == KeyCode.R && event.isShortcutDown()) {
                String[] lines = editor.getText().split("\n");
                int line = 1;
                StringBuilder b = new StringBuilder();
                for (int i = 0; i < lines.length; i++) {
                    if (lines[i].contains(": ")) {
                        String[] split = lines[i].split(": ", 2);
                        b.append(line);
                        b.append(": ");
                        if (split.length == 2) {
                            b.append(split[1]);
                        }
                    } else {
                        b.append(line).append(": ").append(lines[i]);
                    }
                    line++;
                    if (i < lines.length - 1) {
                        b.append("\n");
                    }
                }
                editor.setText(b.toString());
                if (editor.getCaretPosition() == 0) {
                    editor.positionCaret(editor.getText().length());
                }
                return;
            }
        });
    }

    @FXML
    public void onResetAction(ActionEvent e) {
        reset();
    }

    public void reset() {
        fullBtn.setText("Durchlauf");
        error.setText("");
        bz.setText("0");
        akk.setText("0");
        for (Label r : regs) {
            r.setText("-/-");
        }
        editor.setVisible(true);
        code_run_scroll.setVisible(false);
        stepBtn.setText("Start");
    }

    public void load() {
        error.setText("");
        bz.setText("0");
        akk.setText("0");
        for (Label r : regs) {
            r.setText("-/-");
        }
        editor.setVisible(false);
        code_run_scroll.setVisible(true);
        String[] lines = editor.getText().split("\n");
        commands = new Command[lines.length];
        cmd_display = new Label[lines.length];
        code_run.getChildren().clear();
        code_run.setVisible(true);
        int line = 0;
        for (String l : lines) {
            Label update = new Label(l);
            code_run.getChildren().add(update);
            cmd_display[line] = update;
            try {
                if (l.contains(": ")) {
                    String[] parse1 = l.split(": ");

                    int i = Integer.parseInt(parse1[0]);
                    if (i != line + 1) {
                        throw new Exception("Falsche Nummerierung! Formatieren mit STRG+R");
                    }
                    if (parse1.length < 2) {
                        throw new Exception();
                    }
                    String[] pp2 = parse1[1].split("--");
                    String[] pp3 = pp2[0].split(" ");
                    CommandType t = Command.CommandType.safeValueOf(pp3[0]);
                    if (t != null) {
                        if (!t.doesRequireArgument()) {
                            commands[line] = new Command(t, 0);
                        } else if (pp3.length > 1) {
                            commands[line] = new Command(t, Integer.parseInt(pp3[1]));
                        } else {
                            throw new Exception("Fehlendes Argument!");
                        }
                        if (pp3.length > 2) {
                            update.setText(update.getText() + " | Warnung: Unkommentierter Text");
                            update.setId("warning");
                        }
                        System.out.println("Command: " + commands[line].getType());
                        System.out.println("Argument: " + commands[line].getArgument());
                    } else {
                        throw new Exception("Befehl nicht gefunden!");
                    }

                }
            } catch (Exception ex) {
                update.applyCss();
                update.setText(update.getText() + " | " + ex.getMessage());
                update.setId("error");
            }
            line++;
            stepBtn.setText("Schritt");
        }
    }

    public void nextStep() {
        int bz = Integer.parseInt(this.bz.getText());
        bz++;
        int akk = Integer.parseInt(this.akk.getText());
        if (bz <= commands.length) {
            Label display = cmd_display[bz - 1];
            if (current != null) {
                current.setBackground(Background.EMPTY);
            }
            current = display;
            current.setBackground(new Background(new BackgroundFill(Color.LIGHTGREEN, new CornerRadii(3), Insets.EMPTY)));
            Command cmd = commands[bz - 1];
            if (cmd == null) {
                return;
            }
            switch (cmd.getType()) {
                case DLOAD:
                    this.akk.setText(cmd.getArgument() + "");
                    bz++;
                    break;
                case STORE:
                    int i = cmd.getArgument();
                    if (i >= 0 && i < regs.length) {
                        regs[i].setText("" + akk);
                    } else {
                        display.setText(display.getText() + " | Ungültiges Register");
                        display.setId("error");
                    }
                    bz++;
                    break;
                case LOAD:
                    int arg = cmd.getArgument();
                    if (arg >= 0 && arg < regs.length) {
                        try {
                            this.akk.setText("" + Integer.parseInt(regs[arg].getText()));
                        } catch (NumberFormatException ex) {
                            display.setText(display.getText() + " | Register enthält keine Daten");
                            display.setId("error");
                        }
                    } else {
                        display.setText(display.getText() + " | Ungültiges Register");
                        display.setId("error");
                    }
                    bz++;
                    break;
                case ADD:
                    int addLoad = cmd.getArgument();
                    if (addLoad >= 0 && addLoad < regs.length) {
                        try {
                            this.akk.setText("" + (akk + Integer.parseInt(regs[addLoad].getText())));
                        } catch (NumberFormatException ex) {
                            display.setText(display.getText() + " | Register enthält keine Daten");
                            display.setId("error");
                        }
                    } else {
                        display.setText(display.getText() + " | Ungültiges Register");
                        display.setId("error");
                    }
                    bz++;
                    break;
                case SUB:
                    int subLoad = cmd.getArgument();
                    if (subLoad >= 0 && subLoad < regs.length) {
                        try {
                            this.akk.setText("" + (akk - Integer.parseInt(regs[subLoad].getText())));
                        } catch (NumberFormatException ex) {
                            display.setText(display.getText() + " | Register enthält keine Daten");
                            display.setId("error");
                        }
                    } else {
                        display.setText(display.getText() + " | Ungültiges Register");
                        display.setId("error");
                    }
                    bz++;
                    break;
                case END:
                    bz = commands.length + 1;
                    error.setText(getResultMessage());
                    break;
                case MULT:
                    int multLoad = cmd.getArgument();
                    if (multLoad >= 0 && multLoad < regs.length) {
                        try {
                            this.akk.setText("" + (akk * Integer.parseInt(regs[multLoad].getText())));
                        } catch (NumberFormatException ex) {
                            display.setText(display.getText() + " | Register enthält keine Daten");
                            display.setId("error");
                        }
                    } else {
                        display.setText(display.getText() + " | Ungültiges Register");
                        display.setId("error");
                    }
                    bz++;
                    break;
                case DIV:
                    int divLoad = cmd.getArgument();
                    if (divLoad >= 0 && divLoad < regs.length) {
                        try {
                            this.akk.setText("" + (int) (akk / Integer.parseInt(regs[divLoad].getText())));
                        } catch (NumberFormatException ex) {
                            display.setText(display.getText() + " | Register enthält keine Daten");
                            display.setId("error");
                        }
                    } else {
                        display.setText(display.getText() + " | Ungültiges Register");
                        display.setId("error");
                    }
                    bz++;
                    break;
                case JGE:
                    int jump = cmd.getArgument();
                    if (jump >= 1 && jump <= commands.length) {
                        if (akk >= 0) {
                            bz = jump;
                        } else {
                            bz++;
                        }
                    } else {
                        display.setText(display.getText() + " | Ungültiges Sprungziel");
                        display.setId("error");
                        bz++;
                    }
                    break;
                case JGT:
                    jump = cmd.getArgument();
                    if (jump >= 1 && jump <= commands.length) {
                        if (akk > 0) {
                            bz = jump;
                        } else {
                            bz++;
                        }
                    } else {
                        display.setText(display.getText() + " | Ungültiges Sprungziel");
                        display.setId("error");
                        bz++;
                    }
                    break;
                case JLE:
                    jump = cmd.getArgument();
                    if (jump >= 1 && jump <= commands.length) {
                        if (akk <= 0) {
                            bz = jump;
                        } else {
                            bz++;
                        }
                    } else {
                        display.setText(display.getText() + " | Ungültiges Sprungziel");
                        display.setId("error");
                        bz++;
                    }
                    break;
                case JLT:
                    jump = cmd.getArgument();
                    if (jump >= 1 && jump <= commands.length) {
                        if (akk < 0) {
                            bz = jump;
                        } else {
                            bz++;
                        }
                    } else {
                        display.setText(display.getText() + " | Ungültiges Sprungziel");
                        display.setId("error");
                        bz++;
                    }
                    break;
                case JEQ:
                    jump = cmd.getArgument();
                    if (jump >= 1 && jump <= commands.length) {
                        if (akk == 0) {
                            bz = jump;
                        } else {
                            bz++;
                        }
                    } else {
                        display.setText(display.getText() + " | Ungültiges Sprungziel");
                        display.setId("error");
                        bz++;
                    }
                    break;
                case JNE:
                    jump = cmd.getArgument();
                    if (jump >= 1 && jump <= commands.length) {
                        if (akk != 0) {
                            bz = jump;
                        } else {
                            bz++;
                        }
                    } else {
                        display.setText(display.getText() + " | Ungültiges Sprungziel");
                        display.setId("error");
                        bz++;
                    }
                    break;
                case JUMP:
                    jump = cmd.getArgument();
                    if (jump >= 1 && jump <= commands.length) {
                        bz = jump;
                    } else {
                        display.setText(display.getText() + " | Ungültiges Sprungziel");
                        display.setId("error");
                        bz++;
                    }
                    break;
                case TRUMP:
                    bz = commands.length + 1;
                    error.setText("GELB, ORANGE, BOOM!");
                    break;
                default:
                    bz++;
                    break;
            }
            bz--;
            this.bz.setText("" + bz);
        } else {
            error.setText(getResultMessage());
        }

    }

    @FXML
    public void onStepAction(ActionEvent e) {
        if (editor.isVisible()) {
            load();
        } else {
            nextStep();
        }
    }

    @FXML
    public void onFullAction(ActionEvent e) {
        int counter = 1000;
        while (error.getText().isEmpty()) {
            nextStep();
            counter--;
            if (counter == 0) {
                error.setText("Chuck Norris hat das Programm zwei Mal durchlaufen lassen. Er zählt jetzt von unendlich rückwärts.");
                break;
            }
        }
    }

    @FXML
    public void onExport(ActionEvent e) throws FileNotFoundException {
        FileChooser f = new FileChooser();
        f.setTitle("Speicherort wählen...");
        if (loaded != null) {
            f.setInitialDirectory(loaded.getParentFile());
            f.setInitialFileName(loaded.getName());
        }
        File file = f.showSaveDialog(Registermaschine.stage.getOwner());
        if (file != null) {
            if (!file.getName().endsWith(".png")) {
                try {
                    new FileOutputStream(file).write(editor.getText().getBytes("UTF-8"));
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Export erfolgreich");
                    alert.setHeaderText("Der Code wurde erfolgreich exportiert.");
                    alert.getButtonTypes().clear();
                    alert.getButtonTypes().add(ButtonType.OK);
                    alert.showAndWait();
                } catch (IOException ex) {
                    Logger.getLogger(InterfaceController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                WritableImage image = root.snapshot(new SnapshotParameters(), null);
                try {
                    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Export erfolgreich");
                    alert.setHeaderText("Der Screenshot wurde erfolgreich exportiert.");
                    alert.getButtonTypes().clear();
                    alert.getButtonTypes().add(ButtonType.OK);
                    alert.showAndWait();
                } catch (IOException ex) {
                    // TODO: handle exception here
                }
            }
        }
    }

    @FXML
    public void onImport(ActionEvent e) {
        reset();
        FileChooser f = new FileChooser();
        f.setTitle("Zu importierende Datei wählen...");
        f.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Nur Textdateien (.txt)", "*.txt"));
        File file = f.showOpenDialog(Registermaschine.stage.getOwner());
        if (file != null) {
            try {
                BufferedReader r = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
                StringBuilder b = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    b.append(line);
                    b.append(System.lineSeparator());
                }
                editor.setText(b.toString());
                loaded = file;
            } catch (IOException ex) {
                Logger.getLogger(InterfaceController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private int counter = 0;

    private String getResultMessage() {
        Random r = new Random();
        if (r.nextDouble() < 0.07 && System.getProperty("os.name").contains("Windows")) {
            for (int i = 0; i < 3; i++) {
                Alert a = new Alert(Alert.AlertType.ERROR, "Ein Fehler ist passiert!", ButtonType.CANCEL, ButtonType.CLOSE);
                a.setTitle("Windows");
                a.setHeaderText("Fehler");
                a.showAndWait();
            }
            return "Das Programm funktioniert, das Betriebssystem nicht.";
        }
        counter++;
        if (counter == 10) {
            return "Du hast das Programm leider zu oft getestet. Alle Witze wurden aufgebraucht.";
        } else if (counter == 11) {
            return "Oh Entschuldigung, einen hatte ich noch im Cache. Reboot!";
        } else if (counter == 50) {
            return "Du und die Nachrichten feiern goldene Hochzeit! Ziehe 200 Euro ein.";
        }
        String[] messages = new String[]{
            "Das Ende ist dah!",
            "Erfolg! Nichts ist explodiert.",
            "Ich habe fertig!",
            "Wie viele Informatiker braucht man, um eine Glühbirne zu wechseln? - Keinen, das ist ein Hardwareproblem!",
            "Du musstest aber auch alle Register ziehen!",
            "Was sagt ein Informatiker bei Nebel? I don't C#",
            "Ich habe fertig!",
            "Was ist die Lieblingsbeschäftigung von Bits? - Busfahren!",
            "Was macht ein Pirat am Computer? - Er drückt die Enter-Taste",
            "Das Programm war ein (fast) voller Erfolg!",
            "Tipp: Drücke STRG+R, um die Zeilen neu zu formatieren. Mac-Leute können die CMD-Taste statt STRG nutzen. #extrawurscht",
            "MCKetchupDE hat 1 millionen Abos auf YOutube! (Quelle: witze.net)",
            "Tipp: Du kannst den ganzen Rotz auch als Screenshot abspeichern. Einfach als .png exportieren.",
            "640 Kilobyte ought to be enough for anybody. - Bill Gates",
            "Es ist sehr einfach, ein Programm zu schreiben, das innerhalb kürzester Zeit und mit wenig Speicherplatzbedarf ein vorgegebenes Problem NICHT löst.",
            "Tipp: Vergiss nicht zu speichern, sonst war der Mist gratis aber umsonst.",
            "Het programma werd succesvol uitgevoerd!",
            "Das Scripts doch gar nicht!"
        };
        return messages[r.nextInt(messages.length)];
    }

}
